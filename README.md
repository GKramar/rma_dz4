# Bugsy - Čitač za RSS feed Bug.hr #

## Opis aplikacije ##

Bugsy je aplikacija koja omogućuje prikaz vijesti iz RSS *feed-a* na http://www.bug.hr/rss/vijesti/. Za pravilan rad, aplikaciji je potrebna internet veza. Ako ima internet veze, aplikacija bi trebala prikazati vijesti s navedene stranice, točnije sliku, vijesti, vrijeme objave i kategorije. Klikom na element slike, vijest se otvara preko preglednika koji korisnik odabere i prikazuje detaljno. Moguće je i osvježiti stranicu opcijom *pull to refresh*. Također, u *EditText* je moguće unijeti željenu kategoriju i klikom na gumb pored (točnije *ImageButton*) izlistavaju se vijesti iz željene kategorije. Ponovnim osvježvanjem stranice dohvaćaju se sve vijesti i briše se tekst iz *EditText-a*.  

## Rješenja i problemi pri stvaranju aplikacije ##

Prvo je rješeno dohvaćanje teksta sa stranice gdje je za osnovu korišten link [1]. Za dobivanje tekstualnih podataka korišten je *XmlPullParser* dok se dobavljanje xml datoteke s interneta obavlja pomoću *AsyncTaska*,  a rezultati se prikazuju u *RecyclerView-u*. Napravljena je klasa *NewsModel* za vijesti i *NewsListAdapter* vlastiti adapter. Za obradu slike korišten je *Picasso* i link [2]. Za postavljanje *onItemClickListener* na *RecyclerView* nađeno je rješenje [3], a klikom na element liste, dohvaćao se link (ideja kako to napraviti je nađena na [4]) i pozivao implicitni intent. O obzirom da se implicitni intent pozivao iz adapter klase, bilo je potrebno postaviti zastavicu dopuštenja (nađeno na linku [4]). 
Ikona aplikacije je [6].

## Testiranje ##

Testiranje je obavljeno na dva uređaja, prvi je 5.5'' FHD mobilni uređaj (detalji na [7]) i screenshot-ovi su u mapi *Screenshots_DZ4* pod nazivom *test1_X*, a drugi je 5'' HD mobilni uređaj (detalji na [8]) čiji su screenshot-ovi spremljeni pod nazivom *test2_X*.

### Linkovi na vanjske resurse ###

[1] http://www.androidauthority.com/simple-rss-reader-full-tutorial-733245/

[2] https://www.learn2crack.com/2016/02/image-loading-recyclerview-picasso.html

[3] http://stackoverflow.com/questions/24885223/why-doesnt-recyclerview-have-onitemclicklistener-and-how-recyclerview-is-dif

[4] http://stackoverflow.com/questions/32473804/how-to-get-the-position-of-cardview-item-in-recyclerview

[5] http://stackoverflow.com/questions/3918517/calling-startactivity-from-outside-of-an-activity-context

[6] https://www.google.hr/search?client=firefox-b&biw=1536&bih=770&tbm=isch&sa=1&q=ladybug+icon+png&oq=ladybug+icon+png&gs_l=img.3..0i19k1.16660.17745.0.17850.4.2.0.2.2.0.177.318.0j2.2.0....0...1c.1.64.img..0.4.345...0i30i19k1j0i5i30i19k1.u-prq-l9A1g#imgdii=zrgSX5YSpjTtwM:&imgrc=f2GkfNeRNLI5PM:

[7]http://www.devicespecifications.com/en/model/3a353426

[8]http://www.gsmarena.com/lenovo_c2_power-8368.php