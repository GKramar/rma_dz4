package com.example.gabrijela.bugsy;

/**
 * Created by Gabrijela on 26.4.2017..
 */

public class NewsModel {
    public String title;
    public String link;
    public String description;
    public String category;
    public String image;
    public String pubDate;

    public NewsModel(String title, String link, String description, String category, String image, String pubDate) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.category = category;
        this.image = image;
        this.pubDate = pubDate;
    }
}
