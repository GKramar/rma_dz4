package com.example.gabrijela.bugsy;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Gabrijela on 26.4.2017..
 */

public class NewsListAdapter extends RecyclerView.Adapter<NewsListAdapter.NewsViewHolder> {
    private Context context;
    private List<NewsModel> mNewsModels;

    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        private View newsView;

        public NewsViewHolder(View v) {
            super(v);
            newsView = v;
        }
    }

    public NewsListAdapter(List<NewsModel> newsModels, Context context) {
        mNewsModels = newsModels;
        this.context = context;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_news, parent, false);
        NewsViewHolder holder = new NewsViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final NewsViewHolder holder, int position) {
        final NewsModel newsModel = mNewsModels.get(position);
        ((TextView) holder.newsView.findViewById(R.id.tvTitle)).setText(newsModel.title);
        ((TextView) holder.newsView.findViewById(R.id.tvDescription)).setText(newsModel.description);
        ((TextView) holder.newsView.findViewById(R.id.tvCategory)).setText(newsModel.category);
        ((TextView) holder.newsView.findViewById(R.id.tvPubDate)).setText(newsModel.pubDate);
        ImageView ivImage = ((ImageView) holder.newsView.findViewById(R.id.ivImage));
        holder.newsView.setOnClickListener(new MyOnClickListener(position));
        Picasso.with(context)
                .load(newsModel.image)
                .fit()
                .into(ivImage);
    }

    private class MyOnClickListener implements View.OnClickListener {
        int position;

        private MyOnClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            final NewsModel newsObj = mNewsModels.get(position);
            Intent implicitIntent = new Intent();
            String url = newsObj.link;
            Uri uri = Uri.parse(url);
            implicitIntent.setData(uri);
            implicitIntent.setAction(Intent.ACTION_VIEW);
            implicitIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(implicitIntent);
        }
    }

    @Override
    public int getItemCount() {
        return mNewsModels.size();
    }
}

